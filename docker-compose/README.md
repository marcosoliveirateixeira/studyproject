# DOCKER-COMPOSE
https://gist.github.com/jonlabelle/bd667a97666ecda7bbc4f1cc9446d43a

## MANAGEMENT

##### Starts existing containers for a service.
```
docker-compose start
```
##### Stops running containers without removing them.
```
docker-compose stop
```
##### Pauses running containers of a service.
```
docker-compose pause
```
##### Unpauses paused containers of a service.
```
docker-compose unpause
```
##### Lists containers.
```
docker-compose ps
```
##### Builds, (re)creates, starts, and attaches to containers for a service.
```
docker-compose up
```
##### Recreate the docker-compose forcing containers rebuild.
```
docker-compose up --force-recreate
```
##### Stops containers and removes containers, networks, volumes, and images created by up.
```
docker-compose down
```