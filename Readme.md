# TOOLS

## INFRA AS CODE

#### TERRAFORM
Terraform is an open-source infrastructure as code software tool that provides a consistent CLI workflow to manage hundreds of cloud services

#### TERRATEST
Terratest is a Go library that provides patterns and helper functions for testing infrastructure, with 1st-class support for Terraform, Packer, Docker, Kubernetes, AWS, GCP, and more.

#### TERRAGRUNT
Terragrunt is a thin wrapper that provides extra tools for keeping your configurations DRY, working with multiple Terraform modules, and managing remote state.

#### ANSIBLE
Ansible is a radically simple IT automation system. It handles configuration management, application deployment, cloud provisioning, ad-hoc task execution, network automation, and multi-node orchestration.

#### PACKER
Packer is a free and open source tool for creating images for multiple platforms from a single source configuration.

## CONTAINER

#### DOCKER
Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly

#### DOCKER COMPOSE
Docker Compose is a tool that was developed to help define and share multi-container applications

#### KUBERNETES
Kubernetes, or k8s, is an open source platform that automates Linux container operations. It eliminates many of the manual processes involved in deploying and scaling containerized applications
## CONTINUOUS INTEGRATION

#### GIT
Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

#### GITLAB
GitLab is a git-based software repository manager with Wiki, Task Management and CI/CD support. GitLab is similar to GitHub, but GitLab allows developers to store code on their own servers, rather than on third-party servers.
#### CI/CD
CI/CD, continuous integration/continuous delivery, is a method of frequently delivering applications to customers. For this, automation is applied in the application development stages. The main concepts attributed to this method are continuous integration, delivery and deployment. With CI/CD, it is possible to solve the problems that the integration of new codes can cause for the operations and development teams

#### PROMETHEUS 
Prometheus is an open-source systems monitoring and alerting toolkit originally built at SoundCloud

#### GRAFANA
Grafana is an open source solution for running data analytics, pulling up metrics that make sense of the massive amount of data & to monitor our apps with the help of cool customizable dashboards.
