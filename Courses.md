# !IMPORTANTE! SIGA OS CANAIS ABAIXO POIS SÃO OS CANAIS QUE MAIS FORMENTAM A CULTURA DEVOPS NO BRASIL
https://www.youtube.com/user/linuxtipscanal -> Jefferson Fernando (linuxtips)

https://www.youtube.com/c/CaioDelgadoNew -> Caio Delgado

https://www.youtube.com/c/RafaelGomex -> Rafael Gomes (gomex)

https://www.youtube.com/c/PunkdoDevOps -> Camila (punk do devops)

#### São Indicações, lembrem-se que sempre é necessário buscar a tecnologia de sua preferencia :D

# COURSES INDICATION

## Programming Language

#### Python
https://www.udemy.com/course/python-3-do-zero-ao-avancado/ -> This course have a little cost(buy on udemy promotions)

https://www.udemy.com/course/intro_python/ -> This course is free


#### Go
https://www.udemy.com/course/curso-go/ -> This course have a little cost(buy on udemy promotions)

https://www.udemy.com/course/curso-programacao-web-go/ -> This course is free

#### Node

https://www.udemy.com/course/nodejs-do-zero-a-maestria-com-diversos-projetos/ -> This course have a little cost(buy on udemy promotions)

https://www.udemy.com/course/curso-web/  -> This course have a little cost(buy on udemy promotions)

## DevOps Culture

https://4linux.com.br/cursos/treinamento/devops-essentials/ -> This course is free

https://www.youtube.com/watch?v=Z_GTtWzoHOA -> It's a Youtube Video that explore some DevOps Tools


## Cloud

#### AWS

https://www.linuxtips.io/products/treinamento-aws-expert -> This course have a little cost

https://learn.cantrill.io/courses -> This course have a little cost

https://www.udemy.com/course/certificacao-amazon-aws-2019-solutions-architect/ -> This course have a little cost(buy on udemy promotions)
