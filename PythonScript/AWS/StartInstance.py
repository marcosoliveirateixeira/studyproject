
import boto3
region = 'us-west-2'
ec2 = boto3.client('ec2', region_name=region)

def lambda_handler(event, context):
    reservations = ec2.describe_instances(
        Filters=[
            {
                'Name': 'tag:StartStopInstance',
                'Values': [
                    'true'
                ]
            }
        ]
    ).get("Reservations")
    for reservation in reservations:
        for instance in reservation["Instances"]: ## Each reservation has an embedded Instances array that contains all EC2 instances in the reservation
            instance_id = instance["InstanceId"]
            instance_state = instance["State"]["Name"]
            if instance_state == "stopped":
                ec2.start_instances(InstanceIds=[str(instance_id)])
                print(f"Turning On Instance:{instance_id}")
