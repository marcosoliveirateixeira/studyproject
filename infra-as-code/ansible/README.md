# ANSIBLE

### Playbooks

##### Check connectivity of hosts
```
ansible <group> -m ping
```
##### Rebooting hosts 
```
ansible <group> -a “/bin/reboot”             
```
##### Check host system’s info 
```
ansible<group> -m steup | less
```
##### Transfering files
```
ansible <group> -m copy -a “src=home/ansible dest=/tmo/home”
```
##### Create new user
```
ansible<group> -m user -a “name=ansible password= <encrypted password>”
```
##### Deleting user
```
ansible<group> -m user -a “name=ansible state- absent”
```
##### Check if package is installed and update it
```
ansible <group> -m yum -a “name=httpd state=latest”
```
##### Check if package is installed and dont update it
```
ansible<group> -m yum -a “name=httpd state=present”
```
##### Check if package is specific version
```
ansible<group> -m yum -a “name=httpd1.8 state=latest”
```
##### Check if package is not installed
```
ansible <group> -m yum -a “name=httpd state= absent
```

##### Starting a service
```
ansible<group> -m service -a “name=httpd state=”started”
```

##### Stopping a service
```
ansible<group> -m service -a “name=httpd state=”stopped”
```
##### Restarting a service
```
ansible<group> -m service -a “name=httpd state=”restarted
```
### Debugging

##### List facts and state of a host

```
ansible <host> -m setup                            # All facts for one host
```
```
ansible <host> -m setup -a 'filter=ansible_eth*'   # Only ansible fact for one host
```
```
ansible all -m setup -a 'filter=facter_*'          # Only facter facts but for all hosts
```
##### Save facts to per-host files in /tmp/facts

```
ansible all -m setup --tree /tmp/facts
```

####### Working with aws ->
https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html
