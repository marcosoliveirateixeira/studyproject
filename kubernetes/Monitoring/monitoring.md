helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/kube-prometheus-stack
kubectl port-forward prometheus-prometheus-kube-prometheus-prometheus-0 9090:9090 -n default
kubectl port-forward alertmanager-prometheus-kube-prometheus-alertmanager-0 9093:9093 -n default
kubectl port-forward deployment/prometheus-grafana 3000
kubectl expose deployment prometheus-grafana --port=3000 --target-port=3000 --type=LoadBalancer
kubectl get svc
kubectl get pods -o wide
kubectl apply -f deploy-with-error.yaml

####### MÉTRICAS PARA O GRAFANA

# lição de casa: kube_pod_status_reason{namespace="$namespace",reason="Evicted"}
# kube_pod_status_phase{namespace="$namespace",phase="Pending"}

####### SENHA GRAFANA

# usuário: admin
# senha: prom-operator
